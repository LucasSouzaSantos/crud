@extends('layout')

@section('titulo')
    Adicionar
@endsection
@section('cabecalho')

@endsection
@section('pt-principal')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="text-left">Adicionar</h1>
            </div>
            <div class="col-12">
                <form  method="post">
                 @csrf
                <div class="form-row">
                    <div class="col-md-6 mb-3">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="nome" >
                       
                    </div>
                    <div class="col-md-6 mb-6">
                        <label for="name">email:</label>
                        <input type="text" class="form-control" name="email" >
                       
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="name">Senha:</label>
                        <input type="password" class="form-control" name="senha" >
                       
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="name">Data-Nascimento:</label>
                        <input type="date" class="form-control" name="data_nascimento" >
                       
                    </div>
                    <div class="col-12">
                    <button class="btn btn-primary" type="submit">Enviar</button>
                    <button class="btn btn-primary" type="reset">cancel</button>
                    </div>
                </form>
                <div class="col-12">
                                   <!-- carregar erros-->
                            @if ($errors->any())
                                <div class="card-footer">
                                @foreach($errors->all() as $error)
                                    <div class="alert alert-danger">
                                        {{$error}}
                                    
                                    </div>
                                @endforeach 
                                </div>
                                @endif
                </div>
            </div>
        </div>
    </div>
@endsection
@section('rodape')
@endsection