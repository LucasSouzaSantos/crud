@extends('layout')

@section('titulo')
    index
@endsection
@section('cabecalho')
    
@endsection
@section('pt-principal')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h1 class="text-center">Usuarios</h1>
        </div>
        <div class="col-12">
            @if (session('status'))
                <div class="alert alert-success">
            {{ session('status') }}
                 </div>
            @endif
        </div>
        <div class="table-responsive">                
            @if(count($usuario) >0)
            <table class="table table-hover">
                <thead>
                    <tr class="active">
                    <th scope="col">#</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Email</th>
                    <th scope="col">Data Criação</th>
                    <th scope="col-12 " class="text-center" >Acões</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($usuario as $usuarios)
                    <tr class="active">
                    <th scope="row">{{$usuarios->id}}</th>
                    <td>{{$usuarios->nome}}</td>
                    <td>{{$usuarios->email}}</td>
                    <td>{{\Carbon\Carbon::parse($usuarios->created)->format('d/m/Y')}}</td>
                    <td class="botao"><a href="/index/editar/{{$usuarios->id}}" class="btn btn-sm btn-primary bg-light"><img src="{{ asset('img/editar.png') }}" class="img-fluid" alt=""></a>
                    <a href="/index/apagar/{{$usuarios->id}}" class="btn btn-sm btn-danger bg-light excluir-usuario" ><img src="{{ asset('img/excluir.png') }}" class="img-fluid" alt=""></a></td>
                    </tr>
                    @endforeach
                    
                </tbody>
            </table>
            @endif
            <div class="col-12 text-center">
                <a href="/usuario/public/index/add" class="btn btn-sm btn-primary">Adicionar usuario</a></td>
            </div>
        </div>
    </div>
</div>
@endsection
@section('rodape')
@endsection