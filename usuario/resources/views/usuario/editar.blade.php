@extends('layout')

@section('titulo')
    Editar usuario
@endsection

@section('pt-principal')
    <div class="container">
        <div class="row">
        
            <div class="col-12">
            
                <h1 class="text-left">editar</h1>
        
                <form action="/index/editar/{{$usuario->id}}"  method="post">
                 @csrf
                    <div class="form-row">
                        <div class="col-md-4 mb-3">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" name="nome" value="{{$usuario->nome}}" >
                        
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="name">email:</label>
                            <input type="text" class="form-control" name="email" value="{{$usuario->email}}" >
                        
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="name">Senha:</label>
                            <input type="password" class="form-control" name="senha"  >
                        
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="name">Data-Nascimento:</label>
                            <input type="date" class="form-control" name="data_nascimento" value="{{$usuario->data_nascimento}}" >
                        
                        </div>
                    <div class="col-12">
                        <button class="btn btn-primary" type="submit">Salvar</button>
                        <a class="btn btn-primary" href="/" type="reset">Cancelar</a>
                    </div>
                   
                </form>
            </div>
        </div>
    </div>
@endsection
@section('rodape')
@endsection