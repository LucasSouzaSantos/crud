<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','UsuarioController@index');
Route::get('/index','UsuarioController@index');
Route::get('/index/add','UsuarioController@create');
Route::post('/index/add','UsuarioController@store');
Route::get('/index/apagar/{id}','UsuarioController@destroy');
Route::get('/index/editar/{id}','UsuarioController@edit');
Route::post('/index/editar/{id}','UsuarioController@update');
