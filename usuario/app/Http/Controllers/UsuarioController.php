<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuario= Usuario::all();
        return view('usuario.index',compact('usuario'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('usuario.form-cadastro');    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $regras = [
            'nome' => 'required|unique:usuarios',//caso tente cadastrar o mesmo usuario
            'email' => 'required|email',
            'senha' => 'required|min:8'
        ];
        $msg =[
            'required'=>'O campo :atribute é obrigatorio',
            'email.email'=>'Digite um e-mailvalido',
            'senha.min'=>'Senha no mínimo 8 caracteres',
        ];
        //validacao dos campos,antes de salvar no banco
        $request->validate($regras,$msg);
        //se passar na validacao carregar as informacoes no banco
        $cat= new Usuario();
        $cat->nome = $request->input('nome');
        $cat->email = $request->input('email');
        $cat->senha = $request->input('senha');
        $cat->data_nascimento = $request->input('data_nascimento');
        if(asset($cat->save()))
        
        return redirect('/')->with('status', 'Usuário cadastrado com sucesso');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = Usuario::find($id);
        if(isset($usuario)){
            return view('usuario.editar',compact('usuario'));
        }
        return redirect('/index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $usuario = Usuario::find($id);
        if(isset($usuario)){
            $usuario->nome = $request->input('nome');
            $usuario->email= $request->input('email');
            $usuario->data_nascimento = $request->input('data_nascimento');
            $usuario->save();
        }
        return redirect('/index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario = Usuario::find($id);
        if(isset($usuario)){
            $usuario->delete();
        }
        return redirect('/index');
    }
}
